package com.xueliman.iov.resource.data.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author zxg
 */
@Getter
@Setter
public class TestDTO {

    @NotNull(message = "id不能为空")
    private Integer id;

    @NotNull(message = "name不能为空")
    private String name;
}
